﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class UINamesDTO
    {
        public string Name { get; set; }
        public string Region { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
    }
}
