﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class GenderizeDTO
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public double Probability { get; set; }
        public int Count { get; set; }
    }
}
