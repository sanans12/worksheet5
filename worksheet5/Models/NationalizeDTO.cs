﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class NationalizeDTO
    {
        public string Name { get; set; }
        public ICollection<CountryDTO> Country { get; set; }
    }
}
