﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class AgifyDTO
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int Count { get; set; }
    }
}
