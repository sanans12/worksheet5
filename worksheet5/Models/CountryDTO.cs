﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class CountryDTO
    {
        public string Country_Id { get; set; }
        public double Probability { get; set; }
    }
}
