﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace worksheet5.Models
{
    public class RESTCountriesDTO
    {
        public string Name { get; set; }
        public string Alpha2Code { get; set; }
    }
}
