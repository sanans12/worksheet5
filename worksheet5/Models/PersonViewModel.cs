using System.ComponentModel.DataAnnotations;

namespace worksheet5.Models
{
    public class PersonViewModel
    {
        public PersonViewModel()
        { }

        public PersonViewModel(string name, string actualNationality, string actualGender, int actualAge)
        {
            Name = name;
            ActualNationality = actualNationality;
            ActualGender = actualGender;
            ActualAge = actualAge;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string ActualNationality { get; set; }
        public string PredictedNationality { get; set; }

        [Required]
        public string ActualGender { get; set; }
        public string PredictedGender { get; set; }

        [Required]
        public int? ActualAge { get; set; }
        public string PredictedAge { get; set; }
    }
}
