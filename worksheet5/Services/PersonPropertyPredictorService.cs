using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Flurl.Http;
using LazyCache;
using worksheet5.Models;

namespace worksheet5.Services
{
    public class PersonPropertyPredictorService
    {
        /// <summary>
        /// Injected services.
        /// </summary>
        private readonly IMapper _mapper;
        private readonly IAppCache _cache;
        private readonly IGenericFacade _facade;

        /// <summary>
        /// All URLs used.
        /// </summary>
        private const string UINAMES_URL = "https://uinames.com/api/?ext";
        private const string NATIONALIZE_URL = "https://api.nationalize.io/?name=";
        private const string GENDERIZE_URL = "https://api.genderize.io/?name=";
        private const string AGIFY_URL = "https://api.agify.io/?name=";
        private const string RESTCOUNTRIES_URL = "https://restcountries.eu/rest/v2/all?fields=name;alpha2Code";

        public PersonPropertyPredictorService(IMapper mapper, IAppCache cache, IGenericFacade facade)
        {
            _mapper = mapper;
            _cache = cache;
            _facade = facade;
        }

        /// <summary>
        /// Runs a persons name through various APIs to try and predict various details about them
        /// </summary>
        /// <param name="personViewModel"></param>
        /// <returns></returns>
        public async Task<PersonViewModel> CompilePerson(PersonViewModel personViewModel = null)
        {
            PersonViewModel person = personViewModel;

            //Checks if we should ask an outside UI for a random person.
            if (person == null)
            {
                UINamesDTO uiNamesPerson = await _facade.Get<UINamesDTO>(UINAMES_URL);

                person = _mapper.Map<PersonViewModel>(uiNamesPerson);
            }

            //Calls the nationalize API to try and predict the nationality from the name.
            try
            {
                NationalizeDTO nationalizeDTO = await _facade.Get<NationalizeDTO>(NATIONALIZE_URL, person.Name);

                person.PredictedNationality = await AlphaCodeToCountryName(nationalizeDTO.Country.OrderByDescending(i => i.Probability).First().Country_Id);
            }
            catch
            {
                person.PredictedNationality = "Undetermined";
            }

            //Calls the genderize API to try and predict the gender from the name.
            try
            {
                GenderizeDTO genderizeDTO = await _facade.Get<GenderizeDTO>(GENDERIZE_URL, person.Name);

                if (string.IsNullOrWhiteSpace(genderizeDTO.Gender)) throw new NullReferenceException();
                    
                person.PredictedGender = genderizeDTO.Gender;

            }
            catch
            {
                person.PredictedGender = "Undetermined";
            }

            //Calls the agify API to try and predict the age from the name.
            try
            {
                AgifyDTO agifyDTO = await _facade.Get<AgifyDTO>(AGIFY_URL, person.Name);

                person.PredictedAge = agifyDTO.Age.ToString();
            }
            catch
            {
                person.PredictedAge = "Undetermined";
            }

            return person;
        }

        /// <summary>
        /// Gets list of Country Names and their Alpha 2 codes.
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<RESTCountriesDTO>> GetCountriesFromAPI()
        {
            return await _facade.Get<IEnumerable<RESTCountriesDTO>>(RESTCOUNTRIES_URL);
        }

        /// <summary>
        /// Returns a collection of country details after caching them if required.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RESTCountriesDTO>> GetCollectionOfCountries()
        {
            Func<Task<IEnumerable<RESTCountriesDTO>>> countryGetter = () => GetCountriesFromAPI();

            return await _cache.GetOrAddAsync("RESTCountries", countryGetter);
        }

        /// <summary>
        /// Takes an alpha code and outputs the correct country name.
        /// </summary>
        /// <param name="alphaCode"></param>
        /// <returns></returns>
        private async Task<string> AlphaCodeToCountryName(string alphaCode)
        {
            IEnumerable<RESTCountriesDTO> collectionOfCountries = await GetCollectionOfCountries();

            return collectionOfCountries.Single(country => country.Alpha2Code == alphaCode).Name;
        }
    }
}
