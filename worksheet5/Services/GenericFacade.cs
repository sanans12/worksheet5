using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using worksheet5.Models;

namespace worksheet5.Services
{
    public class GenericFacade : IGenericFacade
    {
        public async Task<TDTO> Get<TDTO>(string url, string queryValue = null)
        {
            HttpResponseMessage response = await $"{url}{queryValue}".GetAsync();
            return await response.Content.ReadAsAsync<TDTO>();
        }
    }
}