using System.Threading.Tasks;

namespace worksheet5.Services
{
    public interface IGenericFacade
    {
        Task<TDTO> Get<TDTO>(string url, string queryValue = null);
    }
}