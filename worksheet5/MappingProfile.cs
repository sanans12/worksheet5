﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using worksheet5.Models;

namespace worksheet5
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UINamesDTO, PersonViewModel>().ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                                                    .ForMember(destination => destination.ActualNationality, options => options.MapFrom(source => source.Region))
                                                    .ForMember(destination => destination.ActualGender, options => options.MapFrom(source => source.Gender))
                                                    .ForMember(destination => destination.ActualAge, options => options.MapFrom(source => source.Age));
        }
    }
}
